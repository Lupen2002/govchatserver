var app = require("http").createServer(handler);
var fs = require("fs");
var io = require("socket.io").listen(app);
var logs = [];

app.listen(8124);
io.set('log level', 1);

function handler(req, res) {
    fs.readFile(__dirname + "/index.html", function (err, data) {
        if (err) {
            res.writeHead(500);
            return res.end("Error");
        }
        res.writeHead(200);
        res.end(data);
    });
}

io.sockets.on("connection", function (socket) {
    var name = "no Name";
    logs.forEach(function(msg){
        socket.send(msg);
    })
    socket.on("message", function (msg) {
        name = msg.owner;
        logs.push(msg);
        while(logs.length > 25){
            logs.shift();
        }
        socket.broadcast.send(msg);
    });
    socket.on("disconnect", function () {
        var json = {
            "login": name,
            "date": (new Date).toLocaleTimeString(),
            "text": "Вышел..."
        };
        io.sockets.send(json);
    });
});